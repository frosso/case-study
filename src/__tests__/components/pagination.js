import React from 'react';
import ReactDOM from 'react-dom';

import PaginationContainer from "../../resources/containers/pagination/pagination";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PaginationContainer items={[]} component={() => null} />, div);
});
