import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import ProductActionsComponent from "../../resources/components/product-actions/product-actions";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ProductActionsComponent channel="0" />, div);
});

it('renders both options when available', () => {
  const wrapper = shallow(
    <ProductActionsComponent channel="0" />
  );

  expect(wrapper.find('.js-test-pickup').length).toEqual(1)
  expect(wrapper.find('.js-test-cart').length).toEqual(1)
});

it('renders pickup option when available', () => {
  const wrapper = shallow(
    <ProductActionsComponent channel="2" />
  );

  expect(wrapper.find('.js-test-pickup').length).toEqual(1)
  expect(wrapper.find('.js-test-cart').length).toEqual(0)
});

it('renders buy online option when available', () => {
  const wrapper = shallow(
    <ProductActionsComponent channel="1" />
  );

  expect(wrapper.find('.js-test-pickup').length).toEqual(0)
  expect(wrapper.find('.js-test-cart').length).toEqual(1)
});
