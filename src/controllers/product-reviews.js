import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as productActions from '../store/product/actions';

import Head from './../resources/components/head/head.component';
import ProductReviewsComponent from '../resources/views/product-reviews/product-reviews';

class ProductReviews extends Component {
  componentDidMount() {
    this.props.getProduct(this.props.match.params.productId);
  }

  render() {
    if (this.props.isLoading) {
      return (<h1>Simulated loading of product...</h1>);
    }

    if (!this.props.product || Object.keys(this.props.product).length < 1) {
      return <h1>404 - Product not found</h1>;
    }

    return (
      <div>
        <Head
          title={`Reviews of ${this.props.product.title}`}
        />
        <ProductReviewsComponent product={this.props.product} />
      </div>
    );
  }
}

export default connect(
  (store) => ({
    isLoading: store.productStore.isLoading,
    product: store.productStore.product,
  }),
  {
    getProduct: productActions.getProduct,
  },
)(ProductReviews);
