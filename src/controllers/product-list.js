import React, { Component } from 'react';
import { connect } from 'react-redux';

import Head from './../resources/components/head/head.component';

import * as productActions from '../store/product/actions';
import ProductListComponent from "../resources/views/product-list/product-list";

class ProductList extends Component {
  componentDidMount() {
    this.props.getProducts();
  }

  render() {
    if (this.props.isLoading) {
      return null;
    }

    return (
      <div>
        <Head title="Product List" />
        <ProductListComponent products={this.props.products} />
      </div>
    );
  }
}

export default connect(
  (store) => ({
    products: store.productStore.products,
    isLoading: store.productStore.isLoading,
  }),
  {
    getProducts: productActions.getProducts,
  },
)(ProductList);
