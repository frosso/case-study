import React, { Component } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';

import * as productActions from '../store/product/actions';

import Head from './../resources/components/head/head.component';
import ProductDetailsComponent from '../resources/views/product-details/product-details';

class ProductDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      quantity: 1,
    };

    this.handleQtyBlur = this.handleQtyBlur.bind(this);
    this.handleQtyIncrement = this.handleQtyIncrement.bind(this);
    this.handleQtyDecrement = this.handleQtyDecrement.bind(this);
    this.handleQtyChange = this.handleQtyChange.bind(this);
  }

  componentDidMount() {
    this.props.getProduct(this.props.match.params.productId);
  }

  handleQtyIncrement() {
    this.setState({quantity: this.state.quantity + 1});
  }

  handleQtyDecrement() {
    if (this.state.quantity <= 1) {
      return;
    }

    this.setState({quantity: this.state.quantity - 1});
  }

  handleQtyChange(value) {
    this.setState({quantity: value});
  }

  handleQtyBlur() {
    if (this.state.quantity > 1) {
      return;
    }

    this.setState({quantity: 1});
  }

  render() {
    if (this.props.isLoading) {
      return (<h1>Simulated loading of product...</h1>);
    }

    if (!this.props.product || Object.keys(this.props.product).length < 1) {
      return <h1>404 - Product not found</h1>;
    }

    return (
      <div>
        <Head
          title={this.props.product.title}
          scripts={[{
            type: 'application/ld+json',
            innerHTML: JSON.stringify({
              '@context': 'http://schema.org',
              '@type': 'Product',
              name: this.props.product.title,
              description: get(this.props.product, `shortDescription`),
              sku: this.props.product.UPC,
              image: get(this.props.product, 'Images.0.AlternateImages', []).map(im => im.image)
                .concat(get(this.props.product, 'Images.0.PrimaryImage', []).map(im => im.image))
                .filter(url => !!url),
              offers: {
                '@type': 'Offer',
                price: get(this.props.product, 'Offers.0.OfferPrice.0.priceValue'),
                priceCurrency: get(this.props.product, 'Offers.0.OfferPrice.0.currencyCode'),
              },
              aggregateRating: {
                "@type": "AggregateRating",
                ratingValue: get(this.props.product, 'CustomerReview.0.consolidatedOverallRating'),
                reviewCount: get(this.props.product, 'CustomerReview.0.totalReviews'),
              },
            })
          }]}
        />
        <ProductDetailsComponent
          product={this.props.product}
          quantity={{
            quantity: this.state.quantity,
            handleDec: this.handleQtyDecrement,
            handleInc: this.handleQtyIncrement,
            handleChange: this.handleQtyChange,
            handleBlur: this.handleQtyBlur,
          }}
        />
      </div>
    );
  }
}

export default connect(
  (store) => ({
    isLoading: store.productStore.isLoading,
    product: store.productStore.product,
  }),
  {
    getProduct: productActions.getProduct,
  },
)(ProductDetails);
