// for IE10, I had to add the polyfill. Ideally, we should add it to webpack
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory'
import createRoutes from './routes';
import configureStore from './store';
import registerServiceWorker from './registerServiceWorker';

const history = createHistory();

const store = configureStore({}, history);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      {createRoutes(history)}
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
