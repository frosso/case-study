import * as constants from './constants';

import productData from './item-data.json';

export function getProduct(id) {
  return (dispatch) => {
    dispatch({
      type: constants.SET_LOADING_STATE,
    });

    // TODO: fetch from server/api/whatever, rather than from static file
    (new Promise((resolve) => {
      setTimeout(resolve, 0);
    }))
      .then(() => {
        dispatch({
          type: constants.SET_PRODUCT,
          product: productData.CatalogEntryView.find(product => (product.partNumber === id)),
        });
      });
  }
}

export function getProducts() {
  return (dispatch) => {
    dispatch({
      type: constants.SET_LOADING_STATE,
    });

    // TODO: fetch from server/api/whatever, rather than from static file
    (new Promise((resolve) => {
      setTimeout(resolve, 0);
    }))
      .then(() => {
        dispatch({
          type: constants.SET_PRODUCTS,
          products: productData.CatalogEntryView.map(product => ({
            partNumber: product.partNumber,
            title: product.title,
            Images: product.Images,
          })),
        });
      });
  }
}

// just so I can export multiple actions, even though there's only one at the moment
export function foo() {
}
