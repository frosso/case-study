export const SET_LOADING_STATE = '@product/SET_LOADING_STATE';
export const SET_PRODUCT = '@product/SET_PRODUCT';
export const SET_PRODUCTS = '@product/SET_PRODUCTS';
