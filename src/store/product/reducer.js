import * as constants from './constants';

const initialState = {
  isLoading: false,
  product: {},
  products: [],
};

export default function productReducer(state = initialState, action) {
  if (action.type === constants.SET_LOADING_STATE) {
    return {
      ...state,
      isLoading: true,
    };
  }

  if (action.type === constants.SET_PRODUCT) {
    return {
      ...state,
      product: action.product,
      isLoading: false,
    };
  }

  if (action.type === constants.SET_PRODUCTS) {
    return {
      ...state,
      products: action.products,
      isLoading: false,
    };
  }

  return state;
}
