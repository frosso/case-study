import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';

import rootReducer from './root-reducer';

export default function configureStore(initialState, history) {
  const enhancers = [];
  const middleware = [
    thunk,
    routerMiddleware(history)
  ];

  if (process.env.NODE_ENV !== 'production') {
    // only include if dev env, so not importating at the top of the page
    const { createLogger } = require(`redux-logger`);

    const logger = createLogger({
      // only log when in development environment
      predicate: () => process.env.NODE_ENV === 'development',
    });

    middleware.push(logger);

    // dev tools
    // enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f)
    const devToolsExtension = window.devToolsExtension;

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./root-reducer', () => {
      const nextReducer = require('./root-reducer');

      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
