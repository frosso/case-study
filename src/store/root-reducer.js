import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import productReducer from './product/reducer';

export default combineReducers({
  routing: routerReducer,
  productStore: productReducer,
})
