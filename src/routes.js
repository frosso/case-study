import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import MainLayout from './resources/layouts/main-layout';

import ProductList from './controllers/product-list';
import ProductDetails from './controllers/product-details';
import ProductReviews from './controllers/product-reviews';

export default function createRoutes(history) {
  return (
    <Router
      history={history}
      onChange={() => {
        window.scrollTo(0, 0)
      }}
    >
      <Switch>
        <Route exact path="/" render={(props)=> (
          <MainLayout {...props}>
            <ProductList />
          </MainLayout>
        )} />
        <Route exact path="/products/:slug?/p-:productId" render={(props)=> (
          <MainLayout {...props}>
            <ProductDetails {...props} />
          </MainLayout>
        )} />
        <Route exact path="/products/:slug?/p-:productId/reviews" render={(props)=> (
          <MainLayout {...props}>
            <ProductReviews {...props} />
          </MainLayout>
        )} />
      </Switch>
    </Router>
  );
}
