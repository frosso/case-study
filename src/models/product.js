import get from 'lodash/get';

export default class ProductModel {
  data = {};

  constructor(data) {
    this.data = data;
  }

  id() {
    return this.data.partNumber;
  }

  images() {
    return get(this.data, 'Images.0.AlternateImages', [])
      .reduce((images, image) => {
        images.push(image.image);
        return images;
      }, [get(this.data, 'Images.0.PrimaryImage.0.image', '')])
      // the primary image goes as the first item in the array
      // adding the filter just in case there's an image with an empty src or the PrimaryImage is not available
      .filter(image => image)
  }

  title() {
    return get(this.data, 'title', '');
  }

  reviewsData() {
    return get(this.data, 'CustomerReview.0', null);
  }

  reviews() {
    return get(this.data, 'CustomerReview.0.Reviews', []);
  }

  url() {
    return `/products/${
      this.title()
        .toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')            // Trim - from end of text
      }/p-${this.id()}`;
  }

  promotions() {
    return get(this.data, 'Promotions', []);
  }

  features() {
    return get(this.data, 'ItemDescription.0.features', []);
  }
}
