import './main-layout.scss'

function MainLayout(props) {
  return props.children;
}

export default MainLayout;
