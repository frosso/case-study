import React from 'react';
import { Link } from 'react-router-dom';

import ReviewComponent from './../../components/review/review';
import ReviewBlockComponent from './../../components/reviews-block/reviews-block';
import ProductModel from './../../../models/product';

function ProductReviewsComponent(props) {
  const product = new ProductModel(props.product);
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>
            {product.title()}
            <small> | Reviews</small>
          </h1>
          <Link to={product.url()} className="text-red">
            {/*TODO: I should have used an icon or whatever, but this page wasn't even in the designs, so it doesn't really matter*/}
            ◀️ Back to product details page
          </Link>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <ReviewBlockComponent reviews={product.reviewsData()} />
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          {product.reviews().map(review => (
            <div key={review.reviewKey}>
              <ReviewComponent {...review} />
              <hr />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default ProductReviewsComponent;
