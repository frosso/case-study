import React from 'react';
import { Link } from 'react-router-dom';
import PaginationContainer from "../../containers/pagination/pagination";
import ProductModel from "../../../models/product";

// the following component is just a demo for the carousel, I didn't want to create a separate file for it ¯\_(ツ)_/¯
function ImagesScroller(props) {
  return (<img src={props.items[props.current]} onClick={(e) => {
    e.preventDefault();
    props.actions.handleNext();
  }} />);
}

function ProductListComponent(props) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Product List</h1>
          <ul>
            {props.products.map(data => {
              const product = new ProductModel(data);
              return (
                <li key={`product-${product.id()}`}>
                  <PaginationContainer
                    items={product.images()}
                    component={ImagesScroller}
                  />
                  <Link to={product.url()}>
                    {product.title()}
                  </Link>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default ProductListComponent;
