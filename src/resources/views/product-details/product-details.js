import React from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';

import PriceComponent from './../../components/price/price';
import ReviewsBlockComponent from '../../components/reviews-block/reviews-block';
import PromotionsComponent from './../../components/promotions/promotions';
import QuantityComponent from './../../components/quantity/quantity';

import './product-details.scss';
import PaginationContainer from "../../containers/pagination/pagination";
import ProductGalleryComponent from "../../components/product-gallery/product-gallery";
import ProductActionsComponent from "../../components/product-actions/product-actions";
import ProductModel from "../../../models/product";

function ProductDetailsComponent(props) {
  const product = new ProductModel(props.product);
  return (
    <div className="container">
      <div className="row">
        {/* Left -- product title, gallery, reviews (desktop) */}
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-12 text-center">
              <h1 className="product-details__title">{product.title()}</h1>
            </div>
          </div>
          <div className="row product-details__carousel">
            <div className="col-md-12">
              <PaginationContainer
                items={product.images()}
                component={ProductGalleryComponent}
              />
            </div>
          </div>
          <div className="row hidden-xs hidden-sm product-details__reviews">
            <div className="col-md-12">
              <ReviewsBlockComponent reviews={product.reviewsData()} reviewsDetailsLink={`${product.url()}/reviews`} />
            </div>
          </div>
        </div>
        {/* Right: -- actions, reviews (mobile) */}
        <div className="col-md-6">
          <div className="row product-details__price">
            <div className="col-md-12">
              {/* TODO: figure out how this should properly be nested, how many elements there should be, etc */}
              {get(props.product, 'Offers.0.OfferPrice', []).map((offer, key) => <PriceComponent key={`offer-${key}`} {...offer} />)}
            </div>
          </div>
          <hr className="product-details__separator" />
          <div className="row product-details__promotions">
            <div className="col-md-12">
              <PromotionsComponent promotions={product.promotions()} />
            </div>
          </div>
          <hr className="product-details__separator" />
          <div className="row row--gutter-sm product-details__section product-details__quantity">
            <div className="col-sm-6">
              <QuantityComponent {...props.quantity} />
            </div>
            {/* This element is empty just to fix the padding on the right, since the gutter size is smaller on this column */}
            <div className="col-sm-6" />
          </div>
          <div className="row product-details__section product-details__actions">
            <div className="col-md-12">
              <ProductActionsComponent channel={get(props.product, 'purchasingChannelCode', '-1')} />
            </div>
          </div>
          <div className="row row--gutter-sm product-details__section">
            <div className="col-md-12">
              <div className="product-details__legal">
                <h3 className="legal__title">returns</h3>
                <div className="legal__divider" />
                <div className="legal__description">
                  this item must be returned within 30 days of the ship date.
                  See return policy for details. Prices, pomotions, styles and availability may vary by store and online.
                </div>
              </div>
            </div>
          </div>
          {/*NOTE: had to put them in one line on mobile, otherwise they're too big*/}
          <div className="row row--gutter-sm product-details__section product-details__social">
            <div className="col-sm-4">
              <button className="btn btn-block btn-default btn-sm">add to registry</button>
            </div>
            <div className="col-sm-4">
              <button className="btn btn-block btn-default btn-sm">add to list</button>
            </div>
            <div className="col-sm-4">
              <button className="btn btn-block btn-default btn-sm">share</button>
            </div>
          </div>
          <div className="row product-details__highlights product-details__section">
            <div className="col-md-12">
              <h3 className="highlights__title">product highlights</h3>
              <ul>
                {product.features().map((feature, i) => (
                  <li key={`feature-${i}`} dangerouslySetInnerHTML={{__html: feature}} />
                ))}
              </ul>
            </div>
          </div>
          <div className="row hidden-md hidden-lg product-details__reviews">
            <div className="col-md-12">
              <ReviewsBlockComponent reviews={product.reviewsData()} reviewsDetailsLink={`${product.url()}/reviews`} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

ProductDetailsComponent.propTypes = {
  product: PropTypes.shape().isRequired,
  quantity: PropTypes.shape().isRequired,
};

export default ProductDetailsComponent;
