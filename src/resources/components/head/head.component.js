/**
 * Dependencies
 */
import React from 'react';
import Helmet from 'react-helmet';

/**
 * Use helmet to setup a template for head tags for SEO.
 * @param {string} title
 * @param {string} desc
 * @param {string} path
 * @param {string} host
 * @param {array} links
 * @param {array} scripts
 * @param {string} image
 * @constructor
 */

class Head extends React.Component {
  static defaultProps = {
    image: null,
    links: [],
    scripts: [],
    noIndex: false,
  };

  componentDidMount() {
    window.dataLayer = window.dataLayer || [];
  }

  render() {
    const {title, desc, links, scripts, noIndex} = this.props;
    // default title if one is not provided
    const defaultTitle = '- Case Study App';

    // generic meta tags
    const meta = [{name: 'description', content: desc}];

    // generic link tags and apple touch icons
    const faviconLinks = [
    ].concat(links);

    const faviconMeta = [
      {name: 'theme-color', content: '#cc0000'},
    ];

    // If noIndex is true, the page won't be indexed by search engine web crawlers
    // noIndex defaults to false with defaultProps
    const robotsNoIndex = noIndex ? {name: 'robots', content: 'noindex'} : {};

    const helmetScripts = [].concat(scripts);
    return (
      <Helmet
        title={title}
        titleTemplate={`%s ${defaultTitle}`}
        defaultTitle={defaultTitle}
        meta={[
          ...meta,
          ...faviconMeta,
          robotsNoIndex,
        ]}
        link={[...faviconLinks]}
        script={[...helmetScripts]}
      />
    );
  }
}

export default Head;
