import React from 'react';
import PropTypes from 'prop-types';

import styles from './produt-gallery.module.scss';
import CarouselComponent from "../carousel/carousel";

function ProductGalleryComponent(props) {
  return (
    <div className={styles.container}>
      <div className={styles.mainImage}>
        <a href={props.items[props.current]} target="_blank">
          <img src={props.items[props.current]} className="img-responsive" alt="Main product" />
        </a>
      </div>
      <div className={styles.zoomImage}>
        <a href={props.items[props.current]} target="_blank">
          <span className="glyphicon glyphicon-zoom-in" />
          view larger
        </a>
      </div>
      <div className={styles.carousel}>
        <CarouselComponent {...props} />
      </div>
    </div>
  );
}

ProductGalleryComponent.propTypes = {
  current: PropTypes.number.isRequired,
  start: PropTypes.number.isRequired,
  end: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  actions: PropTypes.shape({
    handleSelection: PropTypes.func.isRequired,
    handleNext: PropTypes.func.isRequired,
    handlePrevious: PropTypes.func.isRequired,
  }).isRequired,
};


export default ProductGalleryComponent;
