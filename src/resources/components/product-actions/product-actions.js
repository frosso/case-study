import React from 'react';
import PropTypes from 'prop-types';

function ProductActionsComponent(props) {
  const isAvailableOnline = ['0', '1'].includes(props.channel);
  const isAvailableInStore = ['0', '2'].includes(props.channel);

  if (!isAvailableOnline && !isAvailableInStore) {
    return null;
  }

  return (
    <div className="row row--gutter-sm">
      {isAvailableInStore && (
        <div className={`col-sm-${isAvailableOnline ? '6' : '12'} text-center`}>
          <button className="btn btn-block btn-default js-test-pickup">Pick up in store</button>
          <button className="btn btn-block btn-xs btn-link hidden-xs hidden-sm">find in a store</button>
        </div>
      )}
      {isAvailableOnline && (
        <div className={`col-sm-${isAvailableInStore ? '6' : '12'} text-center`}>
          <button className="btn btn-block btn-primary js-test-cart">Add to cart</button>
        </div>
      )}
    </div>
  );
}

ProductActionsComponent.propTypes = {
  channel: PropTypes.string.isRequired,
};

export default ProductActionsComponent;
