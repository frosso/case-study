import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './quantity.module.scss';

class QuantityComponent extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-xs-12">
          <div className={styles.container}>
            <label className={styles.inputLabel} htmlFor="quantity">quantity:</label>
            <div className={['input-group', styles.controlsContainer].join(' ')}>
            <span className={['input-group-btn', styles.inputGroupBtn].join(' ')}>
              <button
                className={['btn btn-default', styles.actionButtonDecrement].join(' ')}
                type="button"
                onClick={this.props.handleDec}
              >
                &#8211;
              </button>
            </span>
              <input
                type="text"
                name="quantity"
                id="quantity"
                className="form-control"
                value={this.props.quantity}
                onBlur={() => this.props.handleBlur(this.input.value)}
                onChange={() => this.props.handleChange(this.input.value)}
                ref={(input) => (this.input = input)}
              />
              <span className={['input-group-btn', styles.inputGroupBtn].join(' ')}>
              <button
                className={['btn btn-default', styles.actionButtonIncrement].join(' ')}
                type="button"
                onClick={this.props.handleInc}
              >
                &#43;
              </button>
            </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

QuantityComponent.propTypes = {
  quantity: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  handleInc: PropTypes.func.isRequired,
  handleDec: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
};

export default QuantityComponent;
