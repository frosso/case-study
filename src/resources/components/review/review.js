import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import StarRating from './../star-rating/star-rating';

import styles from './review.module.scss';

function ReviewComponent(props) {
  return (
    <div className="row">
      <div className="col-md-12">
        <StarRating value={parseFloat(props.overallRating)} />
        <h4 className={styles.reviewTitle}>
          {props.title}
        </h4>
        <p>{props.review}</p>
        <div className={styles.reviewMeta}>
          <span className={styles.metaName}>{props.screenName}</span>
          &nbsp;
          {/*It would be just great if ths was a timestamp*/}
          <span className={styles.metaDate}>{
            moment.utc(props.datePosted.replace('UTC ', ''), 'ddd MMM DD kk:mm:ss YYYY')
              .format('MMMM D, YYYY')
          }</span>
        </div>
      </div>
    </div>
  );
}

ReviewComponent.propTypes = {
  review: PropTypes.string.isRequired,
  datePosted: PropTypes.string.isRequired,
  screenName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  overallRating: PropTypes.string.isRequired,
};

export default ReviewComponent;
