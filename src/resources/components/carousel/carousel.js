import React from 'react';
import PropTypes from 'prop-types';
import range from 'lodash/range';

import styles from './carousel.module.scss';

function CarouselComponent(props) {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className={styles.container}>
          <a
            className={styles.leftControl}
            href="#previous-image"
            onClick={(e)=> {
              e.preventDefault();
              props.actions.handlePrevious();
            }}
          >
            ‹
          </a>
          <ul className={styles.list}>
            {range(props.start, props.end + 1)
              .map(index => (
                <li
                  key={`carousel-${index}`}
                  className={[(props.current === index ? styles.listItemActive : ''), styles.listItem].join(' ')}
                >
                  <a
                    href="#image"
                    onClick={() => props.actions.handleSelection(index)}
                  >
                    <img src={props.items[index]} alt="Product" className="img-responsive" />
                  </a>
                </li>
              ))}
          </ul>
          <a
            className={styles.rightControl}
            href="#next-image"
            onClick={(e)=> {
              e.preventDefault();
              props.actions.handleNext();
            }}
          >
            ›
          </a>
        </div>
      </div>
    </div>
  );
}

CarouselComponent.propTypes = {
  current: PropTypes.number.isRequired,
  start: PropTypes.number.isRequired,
  end: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  actions: PropTypes.shape({
    handleSelection: PropTypes.func.isRequired,
    handleNext: PropTypes.func.isRequired,
    handlePrevious: PropTypes.func.isRequired,
  }).isRequired,
};

export default CarouselComponent;
