import React from 'react';
import PropTypes from 'prop-types';

import styles from './price.module.scss';

function PriceComponent(props) {
  return (
    <div className="price">
      <span className={styles.price__value}>{props.formattedPriceValue}</span>
      &nbsp;
      <span className={styles.price__qualifier}>{props.priceQualifier}</span>
    </div>
  );
}

PriceComponent.propTypes = {
  formattedPriceValue: PropTypes.string.isRequired,
  priceQualifier: PropTypes.string.isRequired,
};

export default PriceComponent;
