import React from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';

import styles from './promotions.module.scss';

function PromotionsComponent(props) {
  return (
    <ul className="list-unstyled">
      {props.promotions.map((promotion, key) => {
        return (
          <li key={`promo-${key}`}>
            <a href="#todo" className={['text-red', styles.item].join(' ')}>
              {get(promotion, 'Description.0.shortDescription', '')}
            </a>
          </li>
        );
      })}
    </ul>
  );
}

PromotionsComponent.propTypes = {
  promotions: PropTypes.arrayOf(
    PropTypes.shape({
      Description: PropTypes.arrayOf(PropTypes.shape({
        shortDescription: PropTypes.string.isRequired,
        legalDisclaimer: PropTypes.string.isRequired,
      })).isRequired,
    })
  ).isRequired,
};

export default PromotionsComponent;
