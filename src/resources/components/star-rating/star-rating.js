import React from 'react';
import PropTypes from 'prop-types';

import styles from './star-rating.module.scss';

function StarRatingComponent(props) {
  return (
    <div className={props.className}>
      <div aria-hidden={true} className={styles.starsContainer}>
        <div className={styles.unfilled}>
          &#9733;&#9733;&#9733;&#9733;&#9733;
        </div>
        <div className={styles.filled} style={{width: `${(props.value / 5) * 100}%`}}>
          <div>
            &#9733;&#9733;&#9733;&#9733;&#9733;
          </div>
        </div>
      </div>
      <div className="sr-only">
        Rated {props.value} out of 5 stars
      </div>
    </div>
  )
}

StarRatingComponent.defaultProps = {
  className: '',
};

StarRatingComponent.propTypes = {
  value: PropTypes.number.isRequired,
  className: PropTypes.string,
};

export default StarRatingComponent;
