import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import get from 'lodash/get';

import Review from './../review/review';
import StarRating from './../star-rating/star-rating';

import styles from './reviews-block.module.scss';

function ReviewsBlockComponent(props) {
  if (!props.reviews) {
    return null;
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className={styles.heading}>
          <div className={styles.headingFirstColumn}>
            <StarRating value={parseFloat(props.reviews.consolidatedOverallRating)} className={styles.overallStars} />
            overall
          </div>
          {props.reviewsDetailsLink && (
            <div className={styles.headingSecondColumn}>
              <Link to={props.reviewsDetailsLink}>view all {props.reviews.totalReviews} reviews</Link>
            </div>
          )}
        </div>
        <div className={['row', styles.reviewDescriptionsContainer].join(' ')}>
          <div className="col-md-12">
            <div className="row">
              <div className="col-xs-6">
                <h3 className={styles.columnTitle}>
                  Pro<br />
                  <small className={styles.columnSubtitle}>Most helpful 4-5 star reviews</small>
                </h3>
              </div>
              <div className="col-xs-6">
                <h3 className={styles.columnTitle}>
                  Con<br />
                  <small className={styles.columnSubtitle}>Most helpful 1-2 star reviews</small>
                </h3>
              </div>
            </div>
            <hr className={styles.separator} />
            <div className="row">
              <div className="col-xs-6">
                <Review {...get(props.reviews, 'Pro.0')} />
              </div>
              <div className="col-xs-6">
                <Review {...get(props.reviews, 'Con.0')} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

ReviewsBlockComponent.defaultProps = {
  reviews: null,
  reviewsDetailsLink: '',
};

ReviewsBlockComponent.propTypes = {
  reviews: PropTypes.shape({
    Con: PropTypes.arrayOf(PropTypes.shape({
      review: PropTypes.string.isRequired,
      datePosted: PropTypes.string.isRequired,
      screenName: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    })).isRequired,
    Pro: PropTypes.arrayOf(PropTypes.shape({
      review: PropTypes.string.isRequired,
      datePosted: PropTypes.string.isRequired,
      screenName: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    })).isRequired,
    totalReviews: PropTypes.string.isRequired,
    consolidatedOverallRating: PropTypes.string.isRequired,
  }),
  reviewsDetailsLink: PropTypes.string,
};

export default ReviewsBlockComponent;
