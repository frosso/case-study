import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PaginationContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
    };

    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleSelection = this.handleSelection.bind(this);
  }

  handleNext() {
    // prevents overflow
    // if the user clicks "next" but he's at the last item, go to the first item
    if (this.state.index + 1 >= this.props.items.length) {
      this.setState({index: 0});
      return;
    }

    this.setState({index: this.state.index + 1});
  }

  handlePrevious() {
    // prevents overflow
    // if the user clicks "previous" but he's at the first item, go to the last item
    if (this.state.index - 1 < 0) {
      this.setState({index: this.props.items.length - 1});
      return;
    }

    this.setState({index: this.state.index - 1});
  }

  handleSelection(index) {
    // prevents overflows
    if (index < 0) {
      return;
    }
    if (index >= this.props.items.length) {
      return;
    }

    this.setState({index});
  }

  render() {
    let startIndex = this.state.index - this.props.additionalItems;
    let endIndex = this.state.index + this.props.additionalItems;

    // displays an odd number of items: this.props.additionalItems for each side + the current selection
    // i.e.: always display ((this.props.additionalItems*2)+1) items in the carousel
    if (endIndex >= this.props.items.length) {
      endIndex = this.props.items.length - 1;
      startIndex = this.props.items.length - 1 - (this.props.additionalItems * 2);
      startIndex = startIndex < 0 ? 0 : startIndex;
    }

    if (startIndex <= 0) {
      startIndex = 0;
      endIndex = Math.min((this.props.additionalItems * 2), this.props.items.length - 1);
    }

    return (
      <this.props.component
        current={this.state.index}
        start={startIndex}
        end={endIndex}
        items={this.props.items}
        actions={{
          handleSelection: this.handleSelection,
          handleNext: this.handleNext,
          handlePrevious: this.handlePrevious,
        }}
      />
    );
  }
}

PaginationContainer.defaultProps = {
  additionalItems: 1,
};

PaginationContainer.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
  additionalItems: PropTypes.number,
  component: PropTypes.func.isRequired,
};

export default PaginationContainer;
