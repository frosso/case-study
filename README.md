# Target case study

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Installation
* Copy the `.env-sample` file to `.env`
* Run `npm install`
* Run `npm start`

## Description
* I used `create-react-app` to provide a bootstrap for the application.
* I'm using Redux for state management and, for some cases where global state is not necessary, local component's state.
* I used a lite version of Bootstrap 3 as a base framework for CSS, customizing it to only the files I needed to include.
  * I figured for an existing site, there would be a framework used to provide similar scaffolding
* I used SASS as a CSS preprocessor.
* I used a mix of BEM and CSS modules for CSS styles, just to provide an insight of my capabilities
  * I used BEM only in `src/resources/views/product-details/product-details.js`
  * For the rest of the components, I used CSS modules
* I created 3 routes, for demo purposes
  * `/`: list of product
    * Clicking on the image scrolls through the product images, as a demo for my carousel component
  * `/products/p-:productId`: product details page, the main deliverable of the project
  * `/products/p-:productId/reviews`: list of reviews, since I was so proud of my star rating component 😊
* In projects like these, I try to use kinda an MVC approach to structure my code:
  * Controller files are in `src/controllers/`
  * Model files are in `src/models/`
  * View files are in `src/resources/views/`
  * Partial view files are in `src/resources/[components|containers]/`
  * Redux store information is in `/src/store/`
* I created a separate branch, `async-load`, that has some changes to possibly improve the bundle size for a larger application

## Code deployment approach

This is obviously not a complete application. I would assume that most of this code would be part of a larger code base.  
I used `create-react-app` just to get bootstrapped quickly, but for the purposes of an ecommerce site, I would probably use an express server to render the pages requested server-side.

In my ideal scenario, I would use a similar approach to this, for continuous delivery:

* Ticket gets assigned to developer
* Developer branches off `master`, starts developing
* Developer pushes code to github
* Developer opens Pull Request
* Developer compiles PR:
  * Short description of changes
  * Link to ticket/request
  * Description of how the solution was implemented
  * Description of which files have changed and why
  * Steps to test and/or reproduce the issue
  * Possible deployment notes
* Developer assigns PR for review
* PR is built in CircleCI (or other CI tool)
  * CCI reports status of the PR (tests passing, possibly even bundle size change)
  * CCI creates docker container with code, adds information to PR to where to review the code
    * Could be a hashed subdomain environment mirroring `production`, like https://dev-pr-{:id}.myretail.com
  * Result on build status, tests is reported in PR
* PR is reviewed with visual regression tool on different viewports highlighting changes on some key pages (homepage, product details, etc), with tools like https://percy.io/ or `backstopjs`
  * Since the website is very "visual", I like to do tests like these
  * Result is reported in PR
* PR is reviewed by other developer/s
* PR is reviewed by project owner/s
* If changes are requested, developer keeps making changes to the branch, CCI will run tests again and update the environment created in the previous steps
* Once approved, PR is "merged with rebase" (to keep a nice, continuous git tree) or just rebased on top of `master` (in case the git history is important, e.g.: multiple developers collaborated)
* Once a new change goes into `master`, CCI will run tests, deploy, Percy will review for changes, etc.
